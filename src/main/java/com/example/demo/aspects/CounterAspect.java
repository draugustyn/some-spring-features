package com.example.demo.aspects;

import jakarta.annotation.PostConstruct;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.weaver.AjAttribute;
import org.springframework.stereotype.Component;

// https://www.javainuse.com/spring/spring-boot-aop
// https://fullstackdeveloper.guru/2023/01/25/how-to-implement-aspect-oriented-programming-in-spring-boot/?utm_content=cmp-true



@Aspect
@Component
public class CounterAspect {

    private int cnt;
    private int cnt_err;



    @PostConstruct
    public  void InitCnt()
    {
        cnt = 0;
        cnt_err = 0;
    }


    //@AfterReturning ("execution (* com.example.demo.controllers.MyController.info(..))")

    @AfterReturning ("execution (* com.example.demo.controllers.MyController.info(..)) || execution (* com.example.demo.controllers.MyController.info2(..))")
    public void IncRegularCounter(JoinPoint joinPoint) {
        System.out.println("Method name: " +joinPoint.getSignature());
        System.out.println(++this.cnt);
    }

    @AfterThrowing(value = "execution (* com.example.demo.controllers.MyController.info_exception(..))", throwing = "ex")
    public void IncErrorCounter(JoinPoint joinPoint, Exception ex) {
        System.out.println("Method name: " +joinPoint.getSignature());
        System.out.println(ex.getMessage());
        System.out.println(++this.cnt_err);

    }

}
