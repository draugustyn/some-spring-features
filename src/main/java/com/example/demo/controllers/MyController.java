package com.example.demo.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

// https://localhost:8081/demo/gener-exception
// https://localhost:8081/demo/info
// https://localhost:8081/demo/info2

@RestController
public class MyController {

    @GetMapping("/info")
    public String info(){
        return "demo";
    }


    @GetMapping("/info2")
    public String info2(){
        return "demo2";
    }

    @GetMapping("/gener-exception")
    public String info_exception () throws  Exception {

        throw new Exception("MyException");
        //return "info exception";
    }
}
